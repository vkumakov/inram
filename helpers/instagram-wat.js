const request = require('request-promise');

class InstagramAPI {
  constructor() {
    this.url = 'https://instagram.com/';
    this.media = [];
  }

  getUserInfo(nickname, callback) {
    let infoAddress = this.url + nickname + '/?__a=1',
      self = this;
    request.get(infoAddress).then(function(body) {
      body = JSON.parse(body);
      let infoObj = self._userDataProcessing(body.user);
      callback(null, infoObj);
    }).catch(function(err) {
      callback(err);
    });
  }

  _userDataProcessing(userData) {
    return {
      description: userData.biography || '',
      followers: userData.followed_by.count,
      follows: userData.follows.count,
      fullName: userData.full_name || '',
      id: userData.id,
      posts: userData.media.count,
      username: userData.username
    }
  }

  getMedia(nickname, callback) {
    let mediaAddress = this.url + nickname + '/media',
      self = this;
    request.get(mediaAddress).then(function(body) {
      body = JSON.parse(body);
      self.media = body.items;
      callback(null, body.items);
    }).catch(function(err) {
        callback(err);
    });
  }

  getNextMedia(nickname, callback) {
    let nextMediaAddress = this.url + nickname + '/media?max_id=' + this.media[this.media.length - 1].id,
      self = this;
    request.get(nextMediaAddress).then(function(body) {
      body = JSON.parse(body);
      self.media = self.media.concat(body.items);
      callback(null, body.items);
    }).catch(function(err) {
        callback(err);
    });
  }
}

module.exports = InstagramAPI;
