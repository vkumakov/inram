$(document).ready(function() {
  var userNickname;

  $('#get_info_button').click(function() {
    userNickname = $('#nickname').val();
    $('#user_info').html('');
    $('#user_data').html('');
    $('#add_more_images').prop('disabled', false);

    $.get('/users/info', {nickname:userNickname}).done(userInfoProcessing);
    $.get('/users/media', {nickname:userNickname}).done(userMediaProcessing);
  });

  $('#add_more_images').click(function() {
    $.get('/users/next', {nickname:userNickname}).done(userNextMediaProcessing);
  });

  $('#user_data').on('click', 'img', function(e) {
    var id = e.target.id;
    createBigImage(id);
  });
});

function userInfoProcessing(data) {
  var follows = data.follows,
    followers = data.followers,
    posts = data.posts;

  $('#user_info').append($('<span />').html('Follows: ' + follows), $('<br>'), $('<span />').html('Followers: ' + followers), $('<br>'), $('<span />').html('Posts: ' + posts));
}

function userMediaProcessing(data) {
  var image;

  localStorage.setItem('retrieveData', JSON.stringify(data));
  addNewImages(data);
}

function userNextMediaProcessing(data) {
  var image,
    localData = JSON.parse(localStorage.getItem('retrieveData')).concat(data);

  localStorage.setItem('retrieveData', JSON.stringify(data));
  addNewImages(data);
}

function addNewImages(data) {
  var image;

  for (var i = 0; i < data.length; i++) {
    image = '<img id="' + data[i].code + '" src="' + data[i].images.low_resolution.url + '" width="150" height="150">';
    $('#user_data').append($('<div class="js-small-image">').append(image));
  }
}

function createBigImage(id) {
  var data = JSON.parse(localStorage.getItem('retrieveData'));

  for (var i = 0; i < data.length; i++) {
    if (data[i].code == id) {
      var image = $('<img>'),
        comments = $('<span />').html('Comments: ' + data[i].comments.count),
        likes = $('<span />').html('Likes: ' + data[i].likes.count);

      image.attr('src', data[i].images.standard_resolution.url);
      image.attr('width', 600);
      image.attr('height', 600);

      $('body').append($('<div id="big_image_wrapper" class="big-image">').append(likes, $('<br>'), comments, $('<br>'), image));

      $('#big_image_wrapper').click(function() {
        console.log('meh');
        $("#big_image_wrapper").remove();
        $('#wrap').removeClass('blur');
      });

      $('#wrap').addClass('blur');
      break;
    }
  }
}
