var express = require('express');
var router = express.Router();
var http = require('http');
var InstagramAPI = require('../helpers/instagram-wat.js');
var instagramUser = new InstagramAPI();

router.get('/info', function(req, res, next) {
  instagramUser.getUserInfo(req.query.nickname, (err, data) => {
    if (!err) {
      res.send(data);
    } else {
      res.send(err);
    }
  });
});

router.get('/media', function(req, res, next) {
  instagramUser.getMedia(req.query.nickname, (err, data) => {
    if (!err) {
      res.send(data);
    } else {
      res.send(err);
    }
  });
});

router.get('/next', function(req, res, next) {
  instagramUser.getNextMedia(req.query.nickname, (err, data) => {
    if (!err) {
      res.send(data);
    } else {
      res.send(err);
    }
  });
});

module.exports = router;
